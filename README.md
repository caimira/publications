# Modelling airborne transmission of SARS-CoV-2 using CARA: Risk assessment for enclosed spaces

**Abstract**:
The global crisis triggered by the COVID-19 pandemic has highlighted the need for a proper risk assessment of respiratory pathogens in indoor settings. This paper documents the COVID Airborne Risk Assessment (CARA) methodology, to assess the potential exposure of airborne SARS-CoV-2 viruses, with an emphasis on the effect of certain virological and immunological factors in the quantification of the risk. The proposed model is the result of a multidisciplinary approach linking physical, mechanical and biological domains, benchmarked with clinical and experimental data, enabling decision makers or facility managers to perform risk assessments against airborne transmission. The model was tested against two benchmark outbreaks, showing good agreement. The tool was also applied to several everyday-life settings, in particular for the cases of a shared office, classroom and ski cabin. We found that 20\% of infected hosts can emit approximately 2 orders of magnitude more viral-containing particles, suggesting the imporatance of _super-emitters_ in airborne transmission. The use of surgical-type masks provide a 5-fold reduction in viral emissions. Natural ventilation through the opening of windows at all times are effective strategies to decrease the concentration of virions and slightly opening a window in the winter has approximately the same effect as a full window opening during the summer. Although vaccination is an effective protection measure, nonpharmaceutical interventions, which widely reduces the viral density in the air (ventilation, masks), should be actively supported and included early in the risk assessment process. We propose a critical threshold value approach which could be used to define an acceptable risk level in a given indoor setting.

This repostitory is used to run CARA model and store data to reproduce the results of the paper submitted to the Royal Society Interface Focus journal. See 'Guide to reproduce the results' below (https://gitlab.cern.ch/cara/publications/-/blob/master/README.md#guide-to-reproduce-the-results)

# CARA - COVID Airborne Risk Assessment

CARA is a risk assessment tool developed to model the concentration of viruses in enclosed spaces, in order to perform indoor risk assessments and
inform space-management decisions. It can be used to compare the effectiveness of different airborne-related risk mitigation measures.
The model used was submitted for a peer-review publication.

CARA models the concentration profile of potential virions in enclosed spaces with clear and intuitive graphs.
The user can set a number of parameters, including room volume, exposure time, activity type, mask-wearing and ventilation.
The report generated indicates how to avoid exceeding critical concentrations and chains of airborne transmission in spaces such as individual offices, meeting rooms and labs.

The risk assessment tool simulates the long-range airborne spread SARS-CoV-2 virus in a finite volume, assuming a homogenous mixture, and estimates the risk of COVID-19 infection therein.
The results DO NOT include short-range airborne exposure (where the physical distance is a significant factor) nor the other known modes of SARS-CoV-2 transmission.
Hence, the output from this model is only valid when the other recommended public health & safety instructions are observed, such as adequate physical distancing, good hand hygiene and other barrier measures.

Note that this model implies at least one person is infected and shedding viruses into the simulated volume. 
Nonetheless, it is also important to understand that the absolute risk of infection is uncertain, as it will depend on the probability that someone infected attends the event.
The model is most useful for comparing the impact and effectiveness of different mitigation measures such as ventilation, filtration, exposure time, physical activity andthe size of the room, only considering long-range airborne transmission of COVID-19 in indoor settings.

This tool is designed to be informative, allowing the user to adapt different settings and model the relative impact on the estimated infection probabilities.
The objective is to facilitate targeted decision-making and investment through comparisons, rather than a singular determination of absolute risk.
While the SARS-CoV-2 virus is in circulation among the population, the notion of 'zero risk' or 'completely safe scenario' does not exist.
Each event modelled is unique, and the results generated therein are only as accurate as the inputs and assumptions.

## Authors
CARA is a tool developed by CERN - European Council for Nuclear Research (visit https://home.cern/):

Andre Henriques<sup>1</sup>, Luis Aleixo<sup>1</sup>, Marco Andreini<sup>1</sup>, Gabriella Azzopardi<sup>2</sup>, James Devine<sup>3</sup>, Philip Elson<sup>4</sup>, Nicolas Mounet<sup>2</sup>, Markus Kongstein Rognlien<sup>2,6</sup>, Nicola Tarocco<sup>5</sup>, Julian Tang<sup>7</sup>

<sup>1</sup>HSE Unit, Occupational Health & Safety Group, CERN<br>
<sup>2</sup>Beams Department, Accelerators and Beam Physics Group, CERN<br>
<sup>3</sup>Experimental Physics Department, Safety Office, CERN<br>
<sup>4</sup>Beams Department, Controls Group, CERN<br>
<sup>5</sup>Information Technology Department, Collaboration, Devices & Applications Group, CERN<br>
<sup>6</sup>Norwegian University of Science and Technology (NTNU)<br>
<sup>7</sup>Respiratory Sciences, University of Leicester<br>

### citation
A. Henriques, M. Andreini, G. Azzopardi, J. Devine, P. Elson, N. Mounet, M. Kongstein, N. Tarocco, J. Tang. CARA - COVID Airborne Risk Assessment tool. CERN (2021), https://gitlab.cern.ch/cara/

# Guide to reproduce the results
Guide to install or update Python on your machine, clone the repository and run the scripts to reproduce the results. 

## Setup on Windows machine
<details><summary>Steps</summary>

### How to install or update Python and Git on Windows
To check if you already have Python on your machine, first open a command-line application by searching for `PowerShell`.

<details><summary>Click here for a tip to open the PowerShell App: </summary>

- Press the `Win` key.
- Type "PowerShell".
- Press `Enter`.
</details>

With the command line open, type the following command and press `Enter`:

`python --version`

If you see an error or a version different than `3.9`, then you’ll want to upgrade your installation, or install it from scratch. 
To do that, make sure to download a new `python 3.9` version from https://www.python.org/downloads/release/python-397/ and follow the installation steps. **Tip**: Make sure that the option `Add Python to environmental variables` option in in `Advanced settigs` is checked during the installation.

Check weather you have `git` installed on your machine by typing the following command:
`git --version`. If an error is displayed, make sure to download git from https://git-scm.com/downloads and follow the instalation steps. 

### Clone CARA Publications repository - Windows

Download the script file from [here](https://gitlab.cern.ch/cara/publications/-/raw/master/RS-Interface_Focus_paper/scripts/cara_script.sh?inline=false). Open `Downloads` folder and double click on `cara_script.sh`. Note that this process may take several minutes.

</details>

## Setup on macOS machine
<details><summary>Steps</summary>

### How to install or update Python and Git on macOS
To check if you already have Python on your machine, first open a command-line application by searching for `Terminal`.

<details><summary>Click here for a tip to open the Terminal: </summary>

- Press the `Command` + `Space` keys.
- Type "Terminal".
- Press `Enter`.

Alternatively, you can open Finder and navigate to _Applications - Utilities - Terminal_.
</details>

With the command line open, type the following commands:

`python3 --version`

If you see an error or a version different than `3.9`, then you’ll want to upgrade your installation, or install it from scratch. 
To do that, make sure to download a new `python 3.9` version from https://www.python.org/downloads/release/python-397/ and follow the installation steps. **Tip**: Make sure that the option `Add Python to environmental variables` option in in `Advanced settigs` is checked during the installation. 

### Clone CARA Publications repository - macOS

Download the script file from [here](https://gitlab.cern.ch/cara/publications/-/raw/master/RS-Interface_Focus_paper/scripts/cara_script.command?inline=false). Open your command-line application and type the following commands:

```
cd Downloads
```

then

```
chmod 755 cara_script.command
```

Open `Downloads` folder, right click on `cara_script.command` and select `Open`. A permission dialog will be displayed, click `Open`. Note that this process may take several minutes.

</details>

## Obtaining the results
Once the code is properly installed on your machine, you can now run the script to reproduce the results.
Each figure/result in the paper was generated using methods defined in the code. All the methods are documented with their respective `docstrings`.

<!-- ### Reproduce one specific figure -->
Note that by **default** all the results are "commented out" for performance reasons. In order to generate a specific result or figure, uncomment the respective generation code by following the steps below:

- Open the `results_paper.py` file that is located under the `Downloads/publications/RS-Interface_Focus_paper` folder.
**Note1**: For macOS we recommend to use `IDLE` code editor: right click on the file and select `Open with` and choose `IDLE`.
**Note2**: For windows we recommend to simply use `Notepad` or other code editor application for pyhton.


- Uncomment the desired code lines for a specific method, by removing the `#` tag before and after the method call. For example, if you would like to generate `Fig. 2 a)`, uncomment lines 51-59 and `save` the modifications:
```python
############# FIGURES ############

#### Fig. 1 - vl & vR histograms (PDF)
#present_vl_vR_histograms(activity='Seated', mask='No mask')

### Fig. 2 a) and b) - C(t) & vD for classroom
compare_concentration_curves(models = [classroom_model_windows_open_alltimes(mask='No mask', season='Dec', lenght=0.2, vl=symptomatic_vl_frequencies),
                                       classroom_model_windows_closed(mask='No mask', vl=symptomatic_vl_frequencies),
                                       classroom_model_windows_open_periodic(mask='No mask', season='Dec', lenght=0.6, vl=symptomatic_vl_frequencies),
                                       classroom_model_windows_open_alltimes(mask='No mask', season='Jun', lenght=0.6, vl=symptomatic_vl_frequencies)
                                       ],
                            labels = ['Baseline (20cm window, winter)',  'Window closed', '60cm window open during breaks (winter)',
                                       '60cm window open (summer)'],
                            colors = ['royalblue', 'salmon', (0, 0, 0.54, 1), (0, 0., 0.54, 0.4)])
#compare_concentration_curves(models = [classroom_model_windows_open_alltimes(mask='No mask', season='Dec', lenght=0.2, #vl=symptomatic_vl_frequencies),
#                                       classroom_model_HEPA(mask='No mask', season='Jun', lenght=0.2, vl=symptomatic_vl_frequencies),
#                                       classroom_model_windows_open_alltimes(mask='Type I', season='Dec', lenght=0.2, #vl=symptomatic_vl_frequencies)
#                                      ],
#                             labels = ['Baseline (20cm window, winter)', 'HEPA filter (5 ACH)', 'Surgical-type masks (20cm window, winter)'],
#                             colors = ['royalblue', (0, 0., 0.54, 0.4), (0, 0, 0.54, 1)])

#### Fig. 3 - P(I) vs vl for ski cabin
#composite_plot_pi_vs_viral_load(models = [ski_model(mask='No mask', time=20, vl=symptomatic_vl_frequencies),
#                                          ski_model(mask='Type I', time=20, vl=symptomatic_vl_frequencies),
#                                          ski_model(mask='Type I', time=10, vl=symptomatic_vl_frequencies),
#                                       ],

```

- For macOS, simply click F5 (Run Module) to run the code if you are using `IDLE`.  For windows, simply double click on `results_paper.py` that is located under `Downloads/publications/RS-Interface_Focus_paper` to run the script. 

- The desired figure or result is generated and it will be displayed in a new window or directly in the terminal.


# Disclaimer

CARA has not undergone review, approval or certification by competent authorities, and as a result, it cannot be considered as a fully endorsed and reliable tool, namely in the assessment of potential viral emissions from infected hosts to be modelled.

The software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and non-infringement.
In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the software.
