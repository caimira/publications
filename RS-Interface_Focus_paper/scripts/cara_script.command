cd Downloads
git clone https://gitlab.cern.ch/cara/publications.git
cd publications/RS-Interface_Focus_paper/CARA
git submodule init
git submodule update
pip3 install -e .
cd ..
pip3 install pandas
pip3 install tqdm