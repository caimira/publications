""" Title: CARA - COVID Airborne Risk Assessment
Author: A. Henriques et al
Date: 07/10/2021
Code version: 3.0.1
Availability: https://gitlab.cern.ch/cara/publications """

from cara.models import ExposureModel, InfectedPopulation
from model_scenarios_paper import *
from scripts_paper import *
from itertools import product
from dataclasses import dataclass
from cara.monte_carlo.data import symptomatic_vl_frequencies

#############DATA ############

#print('\n<<<<<<<<<<< vR SEATED >>>>>>>>>>>')
#present_vl_vR_histograms(activity='Seated', mask='No mask')
#print('\n<<<<<<<<<<< vR LIGHT ACTIVITY >>>>>>>>>>>')
#present_vl_vR_histograms(activity='Light activity', mask='No mask')
#print('\n<<<<<<<<<<< vR HEAVY ACTIVITY >>>>>>>>>>>')
#present_vl_vR_histograms(activity='Heavy exercise', mask='No mask')

#print('\n<<<<<<<<<<< Peak viral concentration for baseline scenarios >>>>>>>>>>>')
#present_max_concentration_info(models = [office_model_windows_open_alltimes(mask= 'No mask', season= 'Jun', vl=symptomatic_vl_frequencies),
#                                        classroom_model_windows_open_alltimes(mask='No mask', season='Dec', lenght=0.2, vl=symptomatic_vl_frequencies),
#                                        ski_model(mask='No mask', time=20, vl=symptomatic_vl_frequencies)
#                                        ],
#                               labels = ['Shared office', 'Classroom', 'ski cabin'])

#print('\n<<<<<<<<<<< vD for different scenarios >>>>>>>>>>>')
##example scenario for the classroom (user can choose any other)
#compare_concentration_curves(models = [classroom_model_windows_open_alltimes(mask='No mask', season='Dec', lenght=0.2, vl=symptomatic_vl_frequencies)],
#                             labels = ['Baseline (20cm window, winter)'],
#                             colors = ['royalblue'])

#print('\n<<<<<<<<<<< P(I) for different scenarios >>>>>>>>>>>')
##example scenario for the SV Cholare (user can choose any other)
#output_probability_infection(models = [SVChorale_model()],
#                              labels = ['Benchmark scenario: Chorale Outbreak'])

#print('\n<<<<<<<<<<< air exchange value for a given scenario (in ACH) >>>>>>>>>>>')
##example scenario for the office (user can choose any other)
#air_exchange_from_scenarios(models=[office_model_windows_open_periodic(mask='No mask', season='Jun', vl=symptomatic_vl_frequencies)],
#                            labels=['Windows slightly open (20cm) in summer'])

#############FIGURES ############

####Fig. 1 - vl & vR histograms (PDF)
#present_vl_vR_histograms(activity='Seated', mask='No mask')

####Fig. 2 a) and b) - C(t) & vD for classroom
#compare_concentration_curves(models = [classroom_model_windows_open_alltimes(mask='No mask', season='Dec', lenght=0.2, vl=symptomatic_vl_frequencies),
#                                       classroom_model_windows_closed(mask='No mask', vl=symptomatic_vl_frequencies),
#                                       classroom_model_windows_open_periodic(mask='No mask', season='Dec', lenght=0.6, vl=symptomatic_vl_frequencies),
#                                       classroom_model_windows_open_alltimes(mask='No mask', season='Jun', lenght=0.6, vl=symptomatic_vl_frequencies)
#                                       ],
#                            labels = ['Baseline (20cm window, winter)',  'Window closed', '60cm window open during breaks (winter)',
#                                       '60cm window open (summer)'],
#                            colors = ['royalblue', 'salmon', (0, 0, 0.54, 1), (0, 0., 0.54, 0.4)])
#compare_concentration_curves(models = [classroom_model_windows_open_alltimes(mask='No mask', season='Dec', lenght=0.2, vl=symptomatic_vl_frequencies),
#                                       classroom_model_HEPA(mask='No mask', season='Jun', lenght=0.2, vl=symptomatic_vl_frequencies),
#                                       classroom_model_windows_open_alltimes(mask='Type I', season='Dec', lenght=0.2, vl=symptomatic_vl_frequencies)
#                                       ],
#                             labels = ['Baseline (20cm window, winter)', 'HEPA filter (5 ACH)', 'Surgical-type masks (20cm window, winter)'],
#                             colors = ['royalblue', (0, 0., 0.54, 0.4), (0, 0, 0.54, 1)])

####Fig. 3 - P(I) vs vl for ski cabin
#composite_plot_pi_vs_viral_load(models = [ski_model(mask='No mask', time=20, vl=symptomatic_vl_frequencies),
#                                          ski_model(mask='Type I', time=20, vl=symptomatic_vl_frequencies),
#                                          ski_model(mask='Type I', time=10, vl=symptomatic_vl_frequencies),
#                                       ],
#                             labels = ['Baseline (20min ride)', 'Mask (20min ride)', 'Mask (10min ride)'],
#                                show_lines=False,
#                                ski_lines=True)

####Fig. 4 - vR comparison
#compare_viruses_vr()

####Fig. 5 - Generic P(I) vs vl plot
#plot_pi_vs_viral_load(activity='Heavy exercise', expiration='Shouting', mask='No mask')

####Fig. 6 - P(I) vs vD for SV Chorale outbreak scenario
# plot_pi_vs_dose(models = [SVChorale_model(), SVChorale_model(), SVChorale_model(), SVChorale_model(), SVChorale_model(), SVChorale_improved_model()],
#                draw_boxplot = [True, False, False, False, False, True],
#                labels = ['Choral outbreak (real-life)', 'Delta, non vaccinated', 'Delta, vaccinated',
#                          'Alpha, non vaccinated', 'Alpha, vaccinated', 'Choral outbreak modified'],
#                colors = ['royalblue', 'darkviolet', 'darkviolet',
#                          'plum', 'plum', 'navy'],
#                linestyles = ['solid', 'solid', '--', 'solid', '--', '--'],
#                linewidths = [2, 1, 1, 1, 1, 1],
#                transmissibility_factors = [1., 0.51, 0.51, 0.78, 0.78, 1.],
#                host_immunities = [0., 0., 0.79, 0., 0.92, 0.])

####Fig. S.1 - Particle emission concentration size distribution
#particle_emission_from_diameters(expirations=['Breathing', 'Speaking', 'Shouting'],
#                                    colors = ['lightsteelblue', 'wheat', 'darkseagreen'])

####Fig. S.2 - Effect of surgical mask in the outward direction
#surgical_mask_effect()

####Fig. S.4 - Geneva temperature profil
#plot_hourly_temperatures()

####Fig. S.7 - Deposition fraction
#calculate_deposition_factor()

##Fig. S.8 a) - Emission rate benchmark:  while breathing, light activity #
#exposure_model_from_vl_cn(activity='Seated', expiration='Breathing', mask='No mask')

####Fig. S.8 b) - Emission rate benchmark: while speaking, seated #
#exposure_model_from_vl_cn(activity='Seated', expiration='Speaking', mask='No mask')

####Fig. S.9 - vR histograms (CDF)
#generate_cdf_curves()

####Fig. S.10 - C(t) & vD for shared office
# compare_concentration_curves(models = [office_model_windows_open_alltimes(mask= 'No mask', season= 'Jun', vl=symptomatic_vl_frequencies),
#                                       office_model_windows_closed(mask='No mask', vl=symptomatic_vl_frequencies),
#                                       office_model_windows_open_alltimes(mask= 'Type I', season= 'Jun', vl=symptomatic_vl_frequencies),
#                                       ],
#                             labels = ['Baseline (20cm window)', 'Window closed', 'Surgical-type masks & 20cm window open'],
#                             colors = ['royalblue', (0, 0., 0.54, 0.4), (0, 0, 0.54, 1)])

####Fig. S.11 - C(t) & vD for ski cabin
#compare_concentration_curves(models = [ski_model(mask='No mask', time=20, vl=symptomatic_vl_frequencies),
#                                       ski_model(mask='Type I', time=20, vl=symptomatic_vl_frequencies)
#                                       ],
#                             labels = ['Baseline', 'Surgical-type mask'],
#                             colors = ['royalblue', (0, 0., 0.54, 0.4), (0, 0, 0.54, 1)])

####Scenario air exchange value given a time (in hours)
#air_exchange_from_scenarios(models=[office_model_windows_open_periodic(mask='No mask', season='Jun', vl=symptomatic_vl_frequencies)],
#                            labels=['Windows open periodic'])