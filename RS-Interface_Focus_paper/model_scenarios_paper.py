""" Title: CARA - COVID Airborne Risk Assessment
Author: A. Henriques et al
Date: 07/10/2021
Code version: 3.0.1
Availability: https://gitlab.cern.ch/cara/publications """

from cara import models, data
from cara.monte_carlo.data import activity_distributions, viable_to_RNA_ratio_distribution, infectious_dose_distribution, expiration_distributions, mask_distributions, virus_distributions
import cara.monte_carlo as mc
import numpy as np
from cara.monte_carlo.sampleable import CustomKernel
from cara.monte_carlo.data import BLOmodel
import typing
from cara.apps.calculator.model_generator import build_expiration

_VectorisedFloat = typing.Union[float, np.ndarray]

######### Scatter points (data taken: copies per hour) #########

############# Coleman #############
############# Coleman - Breathing #############
coleman_etal_vl_breathing = [np.log10(821065925.4), np.log10(1382131207), np.log10(81801735.96), np.log10(
    487760677.4), np.log10(2326593535), np.log10(1488879159), np.log10(884480386.5)]
coleman_etal_vR_breathing = [127, 455.2, 281.8, 884.2, 448.4, 1100.6, 621]
############# Coleman - Speaking #############
coleman_etal_vl_speaking = [np.log10(70492378.55), np.log10(7565486.029), np.log10(7101877592), np.log10(1382131207),
                           np.log10(821065925.4), np.log10(1382131207), np.log10(
                               81801735.96), np.log10(487760677.4),
                           np.log10(2326593535), np.log10(1488879159), np.log10(884480386.5)]
coleman_etal_vR_speaking = [1668, 938, 319.6, 3632.8, 1243.6,
                           17344, 2932, 5426, 5493.2, 1911.6, 9714.8]

############# Milton et al #############
milton_vl = [np.log10(8.30E+04), np.log10(4.20E+05), np.log10(1.80E+06)]
milton_er = [22, 220, 1120]
############# Milton et al #############

yann_vl = [np.log10(7.86E+07), np.log10(2.23E+09), np.log10(1.51E+10)]
yann_er = [8396.78166, 45324.55964, 400054.0827]

def cn_expiration_distribution(BLO_factors, cn_values):
    """
    Returns an Expiration with an aerosol diameter distribution, defined
    by the BLO factors (a length-3 tuple).
    The total concentration of aerosols is computed by integrating
    the distribution between 0.1 and 30 microns - these boundaries are
    an historical choice based on previous implementations of the model
    (it limits the influence of the O-mode).
    """
    dscan = np.linspace(0.1, 30. ,3000)
    return mc.Expiration(CustomKernel(dscan,
                BLOmodel(BLO_factors, cn_values).distribution(dscan),kernel_bandwidth=0.1),
                BLOmodel(BLO_factors, cn_values).integrate(0.1, 30.))

expiration_BLO_factors = {
    'Breathing': (1., 0., 0.),
    'Speaking':   (1., 1., 1.),
    'Singing':   (1., 5., 5.),
    'Shouting':  (1., 5., 5.),
}

######### Toronto Temperatures ###########

Toronto_hourly_temperatures_celsius_per_hour = {
        "Jan": [ -2.9, -3.0, -3.2, -3.3, -3.3, -3.5, -3.7, -3.8, -3.9, -4.0, -4.1, -4.3, -4.3, -4.3, -4.1, -3.7, -3.2, -2.8, -2.6, -2.3, -2.2, -2.3, -2.6, -2.8],
        "Feb": [ -2.4, -2.6, -2.8, -2.9, -2.9, -3.1, -3.3, -3.4, -3.6, -3.8, -3.9, -4.0, -4.3, -4.2, -3.7, -3.2, -2.6, -2.1, -1.7, -1.5, -1.3, -1.4, -1.6, -2.1],
        "Mar": [ 1.3, 1.0, 0.7, 0.5, 0.4, 0.1, -0.0, -0.2, -0.4, -0.5, -0.7, -0.8, -0.9, -0.3, 0.4, 1.0, 1.6, 2.0, 2.3, 2.7, 2.7, 2.7, 2.4, 1.9],
        "Apr": [ 6.8, 6.5, 6.3, 5.9, 5.7, 5.4, 5.1, 4.9, 4.6, 4.4, 4.2, 4.3, 4.8, 5.5, 6.1, 6.7, 7.1, 7.6, 7.8, 8.1, 8.2, 8.2, 8.0, 7.6 ],
        "May": [ 13.0, 12.6, 12.2, 11.8, 11.5, 11.2, 10.8, 10.5, 10.2, 9.9, 9.8, 10.0, 10.9, 11.6, 12.2, 12.7, 13.2, 13.6, 13.9, 14.3, 14.4, 14.3, 14.2, 13.8],
        "Jun": [ 18.9, 18.2, 17.8, 17.4, 17.0, 16.6, 16.2, 15.9, 15.6, 15.4, 15.3, 15.8, 16.5, 17.3, 17.9, 18.4, 18.9, 19.4, 19.7, 20.1, 20.3, 20.3, 20.1, 19.7],
        "Jul": [ 22.1, 21.4, 20.9, 20.5, 20.0, 19.6, 19.1, 18.9, 18.6, 18.3, 18.1, 18.5, 19.4, 20.3, 21.0, 21.6, 22.2, 22.7, 23.1, 23.4, 23.6, 23.5, 23.3, 22.9],
        "Aug": [ 22.0, 21.4, 21.0, 20.7, 20.3, 20.0, 19.6, 19.3, 19.1, 18.8, 18.5, 18.4, 19.4, 20.3, 21.1, 21.7, 22.2, 22.8, 23.1, 23.4, 23.5, 23.3, 23.1, 22.6],
        "Sep": [ 18.2, 17.8, 17.4, 17.3, 17.0, 16.6, 16.3, 16.0, 15.8, 15.5, 15.4, 15.0, 15.6, 16.6, 17.5, 18.2, 18.7, 19.2, 19.6, 19.8, 19.7, 19.6, 19.2, 18.6],
        "Oct": [ 11.1, 10.9, 10.6, 10.5, 10.2, 10.1, 9.8, 9.7, 9.5, 9.3, 9.2, 9.0, 9.0, 9.7, 10.5, 11.2, 11.7, 12.2, 12.4, 12.6, 12.6, 12.3, 11.8, 11.3],
        "Nov": [ 5.3, 5.1, 5.0, 4.7, 4.6, 4.4, 4.3, 4.2, 4.1, 4.0, 3.9, 3.8, 3.7, 4.0, 4.6, 5.2, 5.7, 6.1, 6.2, 6.4, 6.3, 6.0, 5.5, 5.3],
        "Dec": [ 0.4, 0.3, 0.2, 0.0, -0.1, -0.2, -0.4, -0.5, -0.6, -0.7, -0.8, -0.8, -0.9, -0.9, -0.6, -0.2, 0.3, 0.7, 0.9, 1.1, 1.1, 0.9, 0.6, 0.5]
 }

# Toronto hourly temperatures as piecewise constant function (in Kelvin).
TorontoTemperatures_hourly = {
    month: models.PiecewiseConstant(
        # NOTE:  It is important that the time type is float, not np.float, in
        # order to allow hashability (for caching).
        tuple(float(time) for time in range(25)),
        tuple(273.15 + np.array(temperatures)),
    )
    for month, temperatures in Toronto_hourly_temperatures_celsius_per_hour.items()
}


# Same temperatures on a finer temperature mesh (every 6 minutes).
TorontoTemperatures = {
    month: TorontoTemperatures_hourly[month].refine(refine_factor=10)
    for month, temperatures in Toronto_hourly_temperatures_celsius_per_hour.items()
}

######### Standard exposure models ###########

def exposure_module(activity: str, expiration: str, mask: str):
    if mask == 'No mask':
        exposure_mask = models.Mask.types['No mask']
    else:
        exposure_mask = mask_distributions[mask]

    exposure_mc = mc.ExposureModel(
        concentration_model=mc.ConcentrationModel(
            room=models.Room(volume=100, humidity=0.5),
            ventilation=models.AirChange(
                active=models.SpecificInterval(((0, 24),)),
                air_exch=0.25,
            ),
            infected=mc.InfectedPopulation(
                number=1,
                virus=virus_distributions['SARS_CoV_2'],
                presence=mc.SpecificInterval(((0, 2),)),
                mask=exposure_mask,
                activity=activity_distributions[activity],
                expiration=expiration_distributions[expiration],
                host_immunity=0.,
            ),
        ),
        exposed=mc.Population(
            number=14,
            presence=mc.SpecificInterval(((0, 2),)),
            activity=activity_distributions[activity],
            mask=exposure_mask,
            host_immunity=0.,
        ),
    )
    return exposure_mc

######### Exposure model for specific viral load ###########
def exposure_vl(activity: str, expiration: str, mask: str, vl: float):
    if mask == 'No mask':
        exposure_mask = models.Mask.types['No mask']
    else:
        exposure_mask = mask_distributions[mask]

    exposure_mc = mc.ExposureModel(
        concentration_model=mc.ConcentrationModel(
            room=models.Room(volume=100, humidity=0.5),
            ventilation=models.AirChange(
                active=models.SpecificInterval(((0, 24),)),
                air_exch=0.25,
            ),
            infected=mc.InfectedPopulation(
                number=1,
                virus=mc.SARSCoV2(
                    viral_load_in_sputum=10**vl,
                    infectious_dose=infectious_dose_distribution,
                    viable_to_RNA_ratio=viable_to_RNA_ratio_distribution,
                    transmissibility_factor=0.51,
                ),
                presence=mc.SpecificInterval(((0, 2),)),
                mask=exposure_mask,
                activity=activity_distributions[activity],
                expiration=expiration_distributions[expiration],
                host_immunity=0.,
            ),
        ),
        exposed=mc.Population(
            number=14,
            presence=mc.SpecificInterval(((0, 2),)),
            activity=activity_distributions[activity],
            mask=exposure_mask,
            host_immunity=0.,
        ),
    )
    return exposure_mc

######### Exposure model for specific viral load ###########
def exposure_vl_cn(activity: str, expiration: str, mask: str, vl: float, cn: typing.Tuple[float, float, float]):
    if mask == 'No mask':
        exposure_mask = models.Mask.types['No mask']
    else:
        exposure_mask = mask_distributions[mask]

    exposure_mc = mc.ExposureModel(
        concentration_model=mc.ConcentrationModel(
            room=models.Room(volume=100, humidity=0.5),
            ventilation=models.AirChange(
                active=models.SpecificInterval(((0, 24),)),
                air_exch=0.25,
            ),
            infected=mc.InfectedPopulation(
                number=1,
                virus=models.Virus(
                    viral_load_in_sputum=10**vl,
                    infectious_dose=infectious_dose_distribution,
                    viable_to_RNA_ratio=viable_to_RNA_ratio_distribution,
                    transmissibility_factor=1.,
                ),
                presence=mc.SpecificInterval(((0, 2),)),
                mask=exposure_mask,
                activity=activity_distributions[activity],
                expiration=cn_expiration_distribution(expiration_BLO_factors[expiration], cn),
                host_immunity=0.,
            ),
        ),
        exposed=mc.Population(
            number=14,
            presence=mc.SpecificInterval(((0, 2),)),
            activity=activity_distributions[activity],
            mask=exposure_mask,
            host_immunity=0.,
        ),
    )
    return exposure_mc


########## Baseline scenarios ###########

# Shared office
def office_model_windows_closed(mask: str, vl: _VectorisedFloat):
    if mask == 'No mask':
        exposure_mask = models.Mask.types['No mask']
    else:
        exposure_mask = mask_distributions[mask]

    return mc.ExposureModel(
        concentration_model=mc.ConcentrationModel(
            room=models.Room(volume=50, humidity=0.5),
            ventilation=models.MultipleVentilation(
                (models.AirChange(active=models.PeriodicInterval(period=120, duration=120), air_exch=0.0),
                models.AirChange(active=models.PeriodicInterval(period=120, duration=120), air_exch=0.25))),
            infected=mc.InfectedPopulation(
                number=1,
                presence=mc.SpecificInterval(present_times = ((0, 3.5), (4.5, 9))),
                virus=mc.SARSCoV2(
                    viral_load_in_sputum=vl,
                    infectious_dose=infectious_dose_distribution,
                    viable_to_RNA_ratio=viable_to_RNA_ratio_distribution,
                    transmissibility_factor=0.51,
                ),
                mask=exposure_mask,
                activity=activity_distributions['Seated'],
                expiration=build_expiration({'Speaking': 0.33, 'Breathing': 0.67}),
                host_immunity=0.,
            )
        ),
        exposed=mc.Population(
            number=3,
            presence=mc.SpecificInterval(present_times = ((0, 3.5), (4.5, 9))),
            activity=activity_distributions['Seated'],
            mask=exposure_mask,
            host_immunity=0.,
        )
    )

def office_model_windows_open_periodic(mask: str, season: str, vl: _VectorisedFloat):
    if mask == 'No mask':
        exposure_mask = models.Mask.types['No mask']
    else:
        exposure_mask = mask_distributions[mask]

    if season == 'Jun':
        temp = 298
    else:
        temp = 293

    return mc.ExposureModel(
        concentration_model=mc.ConcentrationModel(
            room=models.Room(volume=50, humidity=0.5),
            ventilation = models.MultipleVentilation(
                ventilations=(
                    models.SlidingWindow(
                        active=models.PeriodicInterval(period=120, duration=10),
                        inside_temp=models.PiecewiseConstant((0., 24.), (temp,)),
                        outside_temp=data.GenevaTemperatures[season],
                        window_height=1.6, 
                        opening_length=0.2,
                    ),
                    models.AirChange(active=models.PeriodicInterval(period=120, duration=120), air_exch=0.25),
                )  
            ),
            infected=mc.InfectedPopulation(
                number=1,
                presence=mc.SpecificInterval(present_times = ((0, 3.5), (4.5, 9))),
                virus=mc.SARSCoV2(
                    viral_load_in_sputum=vl,
                    infectious_dose=infectious_dose_distribution,
                    viable_to_RNA_ratio=viable_to_RNA_ratio_distribution,
                    transmissibility_factor=0.51,
                ),
                mask=exposure_mask,
                activity=activity_distributions['Seated'],
                expiration=build_expiration({'Speaking': 0.33, 'Breathing': 0.67}),
                host_immunity=0.,
            )
        ),
        exposed=mc.Population(
            number=3,
            presence=mc.SpecificInterval(present_times = ((0, 3.5), (4.5, 9))),
            activity=activity_distributions['Seated'],
            mask=exposure_mask,
            host_immunity=0.,
        )
    )

def office_model_windows_open_alltimes(mask: str, season: str, vl: _VectorisedFloat):
    if mask == 'No mask':
        exposure_mask = models.Mask.types['No mask']
    else:
        exposure_mask = mask_distributions[mask]

    if season == 'Jun':
        temp = 298
    else:
        temp = 293

    return mc.ExposureModel(
        concentration_model=mc.ConcentrationModel(
            room=models.Room(volume=50, humidity=0.5),
            ventilation = models.MultipleVentilation(
                ventilations=(
                    models.SlidingWindow(
                        active=models.PeriodicInterval(period=120, duration=120),
                        inside_temp=models.PiecewiseConstant((0., 24.), (temp,)),
                        outside_temp=data.GenevaTemperatures[season],
                        window_height=1.6,
                        opening_length=0.2,
                    ),
                    models.AirChange(active=models.PeriodicInterval(period=120, duration=120), air_exch=0.25),
                )
            ),
            infected=mc.InfectedPopulation(
                number=1,
                presence=mc.SpecificInterval(present_times = ((0, 3.5), (4.5, 9))),
                virus=mc.SARSCoV2(
                    viral_load_in_sputum=vl,
                    infectious_dose=infectious_dose_distribution,
                    viable_to_RNA_ratio=viable_to_RNA_ratio_distribution,
                    transmissibility_factor=0.51,
                ),
                mask=exposure_mask,
                activity=activity_distributions['Seated'],
                expiration=build_expiration({'Speaking': 0.33, 'Breathing': 0.67}),
                host_immunity=0.,
            )
        ),
        exposed=mc.Population(
            number=3,
            presence=mc.SpecificInterval(present_times = ((0, 3.5), (4.5, 9))),
            activity=activity_distributions['Seated'],
            mask=exposure_mask,
            host_immunity=0.,
        )
    )

# classroom
def classroom_model_windows_closed(mask: str, vl: _VectorisedFloat):
    if mask == 'No mask':
        exposure_mask = models.Mask.types['No mask']
    else:
        exposure_mask = mask_distributions[mask]

    return mc.ExposureModel(
        concentration_model=mc.ConcentrationModel(
            room=models.Room(volume=160, humidity=0.3),
            ventilation=models.MultipleVentilation(
                (models.AirChange(active=models.PeriodicInterval(period=120, duration=120), air_exch=0.0),
                 models.AirChange(active=models.PeriodicInterval(period=120, duration=120), air_exch=0.25))),
            infected=mc.InfectedPopulation(
                number=1,
                presence=models.SpecificInterval(((0, 2), (2.5, 4), (5, 7), (7.5, 9))),
                virus=mc.SARSCoV2(
                    viral_load_in_sputum=vl,
                    infectious_dose=infectious_dose_distribution,
                    viable_to_RNA_ratio=viable_to_RNA_ratio_distribution,
                    transmissibility_factor=0.51,
                ),
                mask=exposure_mask,
                activity=activity_distributions['Light activity'],
                expiration=build_expiration('Speaking'),
                host_immunity=0.,
            )
        ),
        exposed=mc.Population(
            number=19,
            presence=models.SpecificInterval(((0, 2), (2.5, 4), (5, 7), (7.5, 9))),
            activity=activity_distributions['Seated'],
            mask=exposure_mask,
            host_immunity=0.,
        )
    )

def classroom_model_windows_open_alltimes(mask: str, season: str, lenght: float, vl: _VectorisedFloat):
    if mask == 'No mask':
        exposure_mask = models.Mask.types['No mask']
    else:
        exposure_mask = mask_distributions[mask]

    if season == 'Jun':
        temp = 298
    else:
        temp = 293

    return mc.ExposureModel(
        concentration_model=mc.ConcentrationModel(
            room=models.Room(volume=160, humidity=0.3),
            ventilation = models.MultipleVentilation(
                ventilations=(
                    models.SlidingWindow(
                        active=models.PeriodicInterval(period=120, duration=120),
                        inside_temp=models.PiecewiseConstant((0., 24.), (temp,)),
                        outside_temp=TorontoTemperatures[season],
                        window_height=1.6,
                        opening_length=lenght,
                    ),
                    models.AirChange(active=models.PeriodicInterval(period=120, duration=120), air_exch=0.25),
                )
            ),
            infected=mc.InfectedPopulation(
                number=1,
                presence=models.SpecificInterval(((0, 2), (2.5, 4), (5, 7), (7.5, 9))),
                virus=mc.SARSCoV2(
                    viral_load_in_sputum=vl,
                    infectious_dose=infectious_dose_distribution,
                    viable_to_RNA_ratio=viable_to_RNA_ratio_distribution,
                    transmissibility_factor=0.51,
                ),
                mask=exposure_mask,
                activity=activity_distributions['Light activity'],
                expiration=build_expiration('Speaking'),
                host_immunity=0.,
            )
        ),
        exposed=mc.Population(
            number=19,
            presence=models.SpecificInterval(((0, 2), (2.5, 4), (5, 7), (7.5, 9))),
            activity=activity_distributions['Seated'],
            mask=exposure_mask,
            host_immunity=0.,
        )
    )

def classroom_model_HEPA(mask: str, season: str, lenght: float, vl: _VectorisedFloat):
    if mask == 'No mask':
        exposure_mask = models.Mask.types['No mask']
    else:
        exposure_mask = mask_distributions[mask]

    if season == 'Jun':
        temp = 298
    else:
        temp = 293

    return mc.ExposureModel(
        concentration_model=mc.ConcentrationModel(
            room=models.Room(volume=160, humidity=0.3),
            ventilation = models.MultipleVentilation(
                ventilations=(
                    models.HEPAFilter(active=models.PeriodicInterval(period=120, duration=120),
                                      q_air_mech=160*5),
                    models.AirChange(active=models.PeriodicInterval(period=120, duration=120), air_exch=0.25),
                )
            ),
            infected=mc.InfectedPopulation(
                number=1,
                presence=models.SpecificInterval(((0, 2), (2.5, 4), (5, 7), (7.5, 9))),
                virus=mc.SARSCoV2(
                    viral_load_in_sputum=vl,
                    infectious_dose=infectious_dose_distribution,
                    viable_to_RNA_ratio=viable_to_RNA_ratio_distribution,
                    transmissibility_factor=0.51,
                ),
                mask=exposure_mask,
                activity=activity_distributions['Light activity'],
                expiration=build_expiration('Speaking'),
                host_immunity=0.,
            )
        ),
        exposed=mc.Population(
            number=19,
            presence=models.SpecificInterval(((0, 2), (2.5, 4), (5, 7), (7.5, 9))),
            activity=activity_distributions['Seated'],
            mask=exposure_mask,
            host_immunity=0.,
        )
    )

def classroom_model_windows_open_periodic(mask: str, season: str, lenght: float, vl: _VectorisedFloat):
    if mask == 'No mask':
        exposure_mask = models.Mask.types['No mask']
    else:
        exposure_mask = mask_distributions[mask]

    if season == 'Jun':
        temp = 298
    else:
        temp = 293

    return mc.ExposureModel(
        concentration_model=mc.ConcentrationModel(
            room=models.Room(volume=160, humidity=0.3),
            ventilation = models.MultipleVentilation(
                ventilations=(
                    models.SlidingWindow(
                        active=models.SpecificInterval(((2, 2.5), (4, 5), (7, 7.5))),
                        inside_temp=models.PiecewiseConstant((0., 24.), (temp,)),
                        outside_temp=data.GenevaTemperatures[season],
                        window_height=1.6,
                        opening_length=lenght,
                    ),
                    models.AirChange(active=models.PeriodicInterval(period=120, duration=120), air_exch=0.28),
                )
            ),
            infected=mc.InfectedPopulation(
                number=1,
                presence=models.SpecificInterval(((0, 2), (2.5, 4), (5, 7), (7.5, 9))),
                virus=mc.SARSCoV2(
                    viral_load_in_sputum=vl,
                    infectious_dose=infectious_dose_distribution,
                    viable_to_RNA_ratio=viable_to_RNA_ratio_distribution,
                    transmissibility_factor=0.51,
                ),
                mask=exposure_mask,
                activity=activity_distributions['Light activity'],
                expiration=build_expiration('Speaking'),
                host_immunity=0.,
            )
        ),
        exposed=mc.Population(
            number=19,
            presence=models.SpecificInterval(((0, 2), (2.5, 4), (5, 7), (7.5, 9))),
            activity=activity_distributions['Seated'],
            mask=exposure_mask,
            host_immunity=0.,
        )
    )

# ski cabin
def ski_model(mask: str, time: float, vl: _VectorisedFloat):
    if mask == 'No mask':
        exposure_mask = models.Mask.types['No mask']
    else:
        exposure_mask = mask_distributions[mask]

    return mc.ExposureModel(
        concentration_model=mc.ConcentrationModel(
            room=models.Room(volume=10, humidity=0.3),
            ventilation=models.MultipleVentilation(
                (models.AirChange(active=models.PeriodicInterval(period=120, duration=120), air_exch=0.0),
                models.AirChange(active=models.PeriodicInterval(period=120, duration=120), air_exch=0.25))),
            infected=mc.InfectedPopulation(
                number=1,
                presence=models.SpecificInterval(((0, time/60),)),
                virus=mc.SARSCoV2(
                    viral_load_in_sputum=vl,
                    infectious_dose=infectious_dose_distribution,
                    viable_to_RNA_ratio=viable_to_RNA_ratio_distribution,
                    transmissibility_factor=0.51,
                ),
                mask=exposure_mask,
                activity=activity_distributions['Moderate activity'],
                expiration=build_expiration('Speaking'),
                host_immunity=0.,
            )
        ),
        exposed=mc.Population(
            number=3,
            presence=models.SpecificInterval(((0, time/60),)),
            activity=activity_distributions['Moderate activity'],
            mask=exposure_mask,
            host_immunity=0.,
        )
    )

########## Benchmark scenarios ###########

def SVChorale_model():
    return mc.ExposureModel(
        concentration_model=mc.ConcentrationModel(
            room=models.Room(volume=810, humidity=0.5),
            ventilation=models.AirChange(
                active=models.PeriodicInterval(period=120, duration=120),
                air_exch=0.7),
            infected=mc.InfectedPopulation(
                number=1,
                presence=models.SpecificInterval(((0, 2.5), )),
                virus=mc.SARSCoV2(
                    viral_load_in_sputum=10**9,
                    infectious_dose=infectious_dose_distribution,
                    viable_to_RNA_ratio=viable_to_RNA_ratio_distribution,
                    transmissibility_factor=1.,
                ),
                mask=models.Mask.types['No mask'],
                activity=activity_distributions['Moderate activity'],
                expiration=build_expiration('Shouting'),
                host_immunity=0.,
            )
        ),
        exposed=mc.Population(
            number=60,
            presence=models.SpecificInterval(((0, 2.5), )),
            activity=activity_distributions['Moderate activity'],
            mask=models.Mask.types['No mask'],
            host_immunity=0.,
        )
    )

def SVChorale_improved_model():
    return mc.ExposureModel(
        concentration_model=mc.ConcentrationModel(
            room=models.Room(volume=810, humidity=0.5),
            ventilation=models.AirChange(
                active=models.PeriodicInterval(period=120, duration=120),
                air_exch=4),
            infected=mc.InfectedPopulation(
                number=1,
                presence=models.SpecificInterval(((0, 2.5), )),
                virus=mc.SARSCoV2(
                    viral_load_in_sputum=10**9,
                    infectious_dose=infectious_dose_distribution,
                    viable_to_RNA_ratio=viable_to_RNA_ratio_distribution,
                    transmissibility_factor=1.,
                ),
                mask=models.Mask.types['Type I'],
                activity=activity_distributions['Moderate activity'],
                expiration=build_expiration('Shouting'),
                host_immunity=0.,
            )
        ),
        exposed=mc.Population(
            number=60,
            presence=models.SpecificInterval(((0, 2.5), )),
            activity=activity_distributions['Moderate activity'],
            mask=models.Mask.types['Type I'],
            host_immunity=0.,
        )
    )

def Bus_model():
    return mc.ExposureModel(
        concentration_model=mc.ConcentrationModel(
            room=models.Room(volume=45, humidity=0.5),
            ventilation=models.AirChange(
                active=models.PeriodicInterval(period=120, duration=120),
                air_exch=1.25),
            infected=mc.InfectedPopulation(
                number=1,
                presence=models.SpecificInterval(((0, 1.67), )),
                virus=mc.SARSCoV2(
                    viral_load_in_sputum=5*10**8,
                    infectious_dose=infectious_dose_distribution,
                    viable_to_RNA_ratio=viable_to_RNA_ratio_distribution,
                    transmissibility_factor=1.,
                ),
                mask=models.Mask.types['No mask'],
                activity=activity_distributions['Seated'],
                expiration=build_expiration('Speaking'),
                host_immunity=0.,
            )
        ),
        exposed=mc.Population(
            number=67,
            presence=models.SpecificInterval(((0, 1.67), )),
            activity=activity_distributions['Seated'],
            mask=models.Mask.types['No mask'],
            host_immunity=0.,
        )
    )
